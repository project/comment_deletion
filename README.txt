INTRODUCTION
------------
This Module delete the comments for the selected content 
types.

If there is no content type selected then you can't execute the
batch for deletion of comments.

comment_delete.module: automatically deletion of comments on node types of 
your choice.

REQUIREMENTS
------------
This module requires comment module. Comment module should be enable.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

CONFIGURATION
------------
Go to configuration link in admin menu.
Configuration -> System -> Comment Deletion
 
USAGE
------------
Configuration -> System -> Comment Deletion
Select one or more content type
Save Configuration
Click on start batch link. 
After deletion of comment message will appear " @count Comments has deleted 
for the selected content type" 

MAINTAINERS
-----------
Current maintainers:
 * Chetan Sharma (chetan-sharma) - https://www.drupal.org/u/chetan-sharma
